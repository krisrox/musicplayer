#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Local music player 1.0
Michal Staruch
"""

import sys
from os.path import expanduser
from PyQt5.QtWidgets import *
from PyQt5.QtMultimedia import *
from PyQt5.QtCore import *


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.stopState = True
        self.currentPlaylist = QMediaPlaylist()
        self.player = QMediaPlayer()
        self.userAction = -1  # 0=stop, 1=play, 2=pause
        self.player.mediaStatusChanged.connect(self.qmp_mediaStatusChanged)
        self.player.stateChanged.connect(self.qmp_stateChanged)
        self.player.positionChanged.connect(self.qmp_positionChanged)
        self.player.volumeChanged.connect(self.qmp_volumeChanged)
        self.player.setVolume(50)
        self.statusBar().showMessage('No Media :: %d' % self.player.volume())  # status bar on bottom
        self.homeScreen()

    def homeScreen(self):

        self.setWindowTitle('Music Player')
        self.createMenubar()
        self.createToolbar()

        controlBar = self.addControls()  # control bar
        centralWidget = QWidget()
        centralWidget.setLayout(controlBar)
        self.setCentralWidget(centralWidget)  # both infoscreen and control bar to central widget

        self.resize(200, 100)  # dimensions of the MainWindow
        self.show()

    def createMenubar(self):
        menubar = self.menuBar()
        filemenu = menubar.addMenu('File')
        filemenu.addAction(self.fileOpen())
        filemenu.addAction(self.info())  # about me :)
        filemenu.addAction(self.exitAction())

    def createToolbar(self):
        pass

    def addControls(self):
        controlArea = QVBoxLayout()  # centralWidget
        seekSliderLayout = QHBoxLayout()
        controls = QHBoxLayout()
        playlistCtrlLayout = QHBoxLayout()

        # creating buttons
        playBt = QPushButton('Play')
        pauseBt = QPushButton('Pause')
        stopBt = QPushButton('Stop')
        volumeDescBt = QPushButton('Vol (-)')
        volumeIncBt = QPushButton('Vol (+)')
        prevBt = QPushButton('Prev Song')
        nextBt = QPushButton('Next Song')

        # seek slider
        seekSlider = QSlider()
        seekSlider.setMinimum(0)
        seekSlider.setMaximum(100)
        seekSlider.setOrientation(Qt.Horizontal)
        seekSlider.setTracking(False)
        seekSlider.sliderMoved.connect(self.seekPosition)

        seekSliderLabel1 = QLabel('0.00')
        seekSliderLabel2 = QLabel('0.00')
        seekSliderLayout.addWidget(seekSliderLabel1)
        seekSliderLayout.addWidget(seekSlider)
        seekSliderLayout.addWidget(seekSliderLabel2)

        # handler for each button not using the default slots
        playBt.clicked.connect(self.playHandler)
        pauseBt.clicked.connect(self.pauseHandler)
        stopBt.clicked.connect(self.stopHandler)
        volumeDescBt.clicked.connect(self.decreaseVolume)
        volumeIncBt.clicked.connect(self.increaseVolume)

        # horizontal layout
        controls.addWidget(volumeDescBt)
        controls.addWidget(playBt)
        controls.addWidget(pauseBt)
        controls.addWidget(stopBt)
        controls.addWidget(volumeIncBt)

        # playlist control button handlers
        prevBt.clicked.connect(self.prevItemPlaylist)
        nextBt.clicked.connect(self.nextItemPlaylist)
        playlistCtrlLayout.addWidget(prevBt)
        playlistCtrlLayout.addWidget(nextBt)

        # vertical layout
        controlArea.addLayout(seekSliderLayout)
        controlArea.addLayout(controls)
        controlArea.addLayout(playlistCtrlLayout)
        return controlArea

    def playHandler(self):
        self.userAction = 1
        self.statusBar().showMessage('Playing at Volume %d' % self.player.volume())
        if self.player.state() == QMediaPlayer.StoppedState:
            if self.player.mediaStatus() == QMediaPlayer.NoMedia:
                print(self.currentPlaylist.mediaCount())
                if self.currentPlaylist.mediaCount() == 0:
                    self.openFile()
                if self.currentPlaylist.mediaCount() != 0:
                    self.player.setPlaylist(self.currentPlaylist)
            elif self.player.mediaStatus() == QMediaPlayer.LoadedMedia:
                self.player.play()
            elif self.player.mediaStatus() == QMediaPlayer.BufferedMedia:
                self.player.play()
        elif self.player.state() == QMediaPlayer.PlayingState:
            pass
        elif self.player.state() == QMediaPlayer.PausedState:
            self.player.play()

    def pauseHandler(self):
        self.userAction = 2
        self.statusBar().showMessage(
            'Paused %s at position %s at Volume %d' % (self.player.metaData(QMediaMetaData.Title),
                                                       self.centralWidget().layout().itemAt(0).layout().itemAt(
                                                           0).widget().text(),
                                                       self.player.volume()))
        self.player.pause()

    def stopHandler(self):
        self.userAction = 0
        self.statusBar().showMessage('Stopped at Volume %d' % (self.player.volume()))
        if self.player.state() == QMediaPlayer.PlayingState:
            self.player.stop()
        elif self.player.state() == QMediaPlayer.PausedState:
            self.player.stop()
        elif self.player.state() == QMediaPlayer.StoppedState:
            pass

    def qmp_mediaStatusChanged(self):
        if self.player.mediaStatus() == QMediaPlayer.LoadedMedia and self.userAction == 1:
            durationT = self.player.duration()
            self.centralWidget().layout().itemAt(0).layout().itemAt(1).widget().setRange(0, durationT)
            self.centralWidget().layout().itemAt(0).layout().itemAt(2).widget().setText(
                '%d:%02d' % (int(durationT / 60000), int((durationT / 1000) % 60)))
            self.player.play()

    def qmp_stateChanged(self):
        if self.player.state() == QMediaPlayer.StoppedState:
            self.player.stop()

    def qmp_positionChanged(self, position, senderType=False):
        sliderLayout = self.centralWidget().layout().itemAt(0).layout()
        if not senderType:
            sliderLayout.itemAt(1).widget().setValue(position)
        # update text label
        sliderLayout.itemAt(0).widget().setText('%d:%02d' % (int(position / 60000), int((position / 1000) % 60)))

    def seekPosition(self, position):
        sender = self.sender()
        if isinstance(sender, QSlider):
            if self.player.isSeekable():
                self.player.setPosition(position)

    def qmp_volumeChanged(self):
        msg = self.statusBar().currentMessage()
        msg = msg[:-2] + str(self.player.volume())
        self.statusBar().showMessage(msg)

    def increaseVolume(self):
        vol = self.player.volume()
        vol = min(vol + 5, 100)
        self.player.setVolume(vol)

    def decreaseVolume(self):
        vol = self.player.volume()
        vol = max(vol - 5, 0)
        self.player.setVolume(vol)

    def fileOpen(self):
        fileAc = QAction('Open File', self)
        fileAc.setShortcut('Ctrl+O')
        fileAc.setStatusTip('Open File')
        fileAc.triggered.connect(self.openFile)
        return fileAc

    def openFile(self):
        fileChoosen = QFileDialog.getOpenFileUrl(self, 'Open Music File', expanduser('~'), 'Audio (*.mp3 *.ogg *.wav)',
                                                 '*.mp3 *.ogg *.wav')
        if fileChoosen is not None:
            self.currentPlaylist.addMedia(QMediaContent(fileChoosen[0]))

    def info(self):
        infoAc = QAction('Info', self)
        infoAc.setShortcut('F1')
        infoAc.setStatusTip('About app')
        infoAc.triggered.connect(self.aboutApp)
        return infoAc

    def aboutApp(self):
        fullText = 'Local Music Player by Michal Staruch'
        infoBox = QMessageBox(self)
        infoBox.setWindowTitle('About App')
        infoBox.setTextFormat(Qt.RichText)
        infoBox.setText(fullText)
        infoBox.addButton('OK', QMessageBox.AcceptRole)
        infoBox.show()

    def prevItemPlaylist(self):
        self.player.playlist().previous()

    def nextItemPlaylist(self):
        self.player.playlist().next()

    def exitAction(self):
        exitAc = QAction('&Exit', self)
        exitAc.setShortcut('Ctrl+Q')
        exitAc.setStatusTip('Exit App')
        exitAc.triggered.connect(self.closeEvent)
        return exitAc

    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Message', 'Do you want to exit?', QMessageBox.Yes | QMessageBox.No,
                                     QMessageBox.Yes)

        if reply == QMessageBox.Yes:
            qApp.quit()
        else:
            try:
                event.ignore()
            except AttributeError:
                pass


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())
